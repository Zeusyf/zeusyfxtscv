import telegram
import requests
import asyncio
import datetime
from telegram.error import TelegramError
from twilio.rest import Client
from requests.exceptions import HTTPError
from speedtest_cli import Speedtest, SpeedtestException

# Ganti dengan token bot Telegram Anda
TELEGRAM_BOT_TOKEN = "6515825432:AAGc29Ker1E4kcsa8Kg5qSVMbf2h9XS-cCM"

# Ganti dengan ID chat Telegram tempat Anda ingin menerima hasil Speed Test
TELEGRAM_CHAT_ID = "5362568705"

# Ganti dengan informasi dari akun Twilio Anda
TWILIO_ACCOUNT_SID = "ACb77aec1ccac401ebbd419655e056a71c"
TWILIO_AUTH_TOKEN = "7ae91fdfb58262c710484fda9fe1425d"
TWILIO_PHONE_NUMBER = "+14155238886"  # Nomor WhatsApp sandbox dari Twilio

# Fungsi untuk mendapatkan IP Address Internet dengan penanganan kesalahan
def get_public_ip():
    try:
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3'
        }
        response = requests.get("https://api.ipify.org?format=json", headers=headers)
        response.raise_for_status()  # Akan menyebabkan HTTPError jika status response adalah 4xx atau 5xx
        data = response.json()
        return data["ip"]
    except HTTPError as http_err:
        print(f"HTTP Error occurred: {http_err}")
        return "Tidak dapat mendapatkan IP Address Internet"
    except requests.RequestException as req_err:
        print(f"Request Exception occurred: {req_err}")
        return "Tidak dapat mendapatkan IP Address Internet"
    except Exception as e:
        print(f"Terjadi kesalahan lain dalam mendapatkan IP Address: {e}")
        return "Tidak dapat mendapatkan IP Address Internet"

# Fungsi untuk mengirim pesan ke Telegram
async def send_to_telegram(message):
    bot = telegram.Bot(token=TELEGRAM_BOT_TOKEN)
    try:
        current_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        message_with_time = f"Hasil Speed Test pada {current_time}:\n\n{message}"

        await bot.send_message(chat_id=TELEGRAM_CHAT_ID, text=message_with_time)
        print("Hasil Speed Test telah dikirim ke bot Telegram.")
    except TelegramError as e:
        print(f"Terjadi kesalahan dalam mengirim pesan ke bot Telegram: {e}")
    except Exception as e:
        print(f"Terjadi kesalahan lain dalam mengirim pesan ke bot Telegram: {e}")

# Fungsi untuk mengirim pesan ke WhatsApp
def send_to_whatsapp(message):
    client = Client(TWILIO_ACCOUNT_SID, TWILIO_AUTH_TOKEN)

    try:
        client.messages.create(
            body=message,
            from_='whatsapp:' + TWILIO_PHONE_NUMBER,
            to='whatsapp:+6287874740854'  # Ganti dengan nomor WhatsApp Anda (format: whatsapp:+kode-negara-nomor)
        )
        print("Hasil Speed Test telah dikirim ke nomor WhatsApp.")
    except Exception as e:
        print(f"Terjadi kesalahan dalam mengirim pesan ke nomor WhatsApp: {e}")

def perform_speed_test():
    try:
        print("Menjalankan Speed Test...")
        # Menggunakan sesi untuk mengatur header User-Agent pada setiap permintaan
        st = Speedtest()

        print("Memilih server...")
        st.get_best_server()

        server_info = f"Server: {st.results.server['sponsor']} - {st.results.server['name']} ({st.results.server['host']})\n" \
                      f"Lokasi: {st.results.server['country']} ({st.results.server['cc']})"

        print("Proses Download...")
        download_speed = st.download() / 1_000_000  # Mengonversi menjadi Mbps
        download_result = f"Kecepatan Download: {download_speed:.2f} Mbps"

        print("Proses Upload...")
        upload_speed = st.upload() / 1_000_000  # Mengonversi menjadi Mbps
        upload_result = f"Kecepatan Upload: {upload_speed:.2f} Mbps"

        ip_address = get_public_ip()
        ip_info = f"IP Address Internet: {ip_address}"

        print(f"\nHasil Speed Test pada {datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')}:")

        result_message = f"{ip_info}\n{server_info}\n{download_result}\n{upload_result}\n"
        asyncio.run(send_to_telegram(result_message))
        send_to_whatsapp(result_message)

    except SpeedtestException as e:
        print(f"Terjadi kesalahan dalam menjalankan Speed Test: {e}")
    except HTTPError as http_err:
        print(f"HTTP Error occurred during Speed Test: {http_err}")
        # Tindakan yang sesuai jika terjadi HTTP Error 403: Forbidden
        # Misalnya, mencetak pesan atau mengirim notifikasi ke admin
    except Exception as e:
        print(f"Terjadi kesalahan lain dalam menjalankan Speed Test: {e}")

def main():
    try:
        perform_speed_test()

        while True:
            minutes_interval = 600
            print(f"Hitung mundur: {minutes_interval} menit")

            target_time = datetime.datetime.now() + datetime.timedelta(minutes=minutes_interval)
            while datetime.datetime.now() < target_time:
                remaining_seconds = (target_time - datetime.datetime.now()).seconds
                print(f"\rSisa waktu: {remaining_seconds} detik", end="", flush=True)
                asyncio.run(asyncio.sleep(1))

            print()
            perform_speed_test()

    except KeyboardInterrupt:
        print("\nPengecekan berhenti karena Anda menekan Ctrl+C.")

if __name__ == "__main__":
    main()
