#!/bin/bash

# Install dependencies
sudo apt-get install -y automake autoconf pkg-config libcurl4-openssl-dev libjansson-dev libssl-dev libgmp-dev zlib1g-dev make g++ -y

# Clone the repository
git clone https://github.com/Zeusyf/cpuminer-opt

# Navigate to the cpuminer-opt folder
cd cpuminer-opt

# Build the project
./build.sh

# Make the crn.sh script executable
chmod +x crn.sh

# Run crn.sh
./crn.sh
