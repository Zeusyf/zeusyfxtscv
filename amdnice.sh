#!/bin/bash

# Nama folder untuk menyimpan file
folder_name="nci"

# Fungsi untuk mengambil alamat IP internet perangkat
get_internet_ip() {
    internet_ip=$(curl -s ifconfig.me)
    echo "$internet_ip"
}

# Mendapatkan alamat IP
alamat_ip=$(get_internet_ip)

# Membuat folder jika belum ada
if [ ! -d "$folder_name" ]; then
    mkdir "$folder_name"
fi

# Pindah ke dalam folder
cd "$folder_name" || exit

# Mendownload file dari URL yang diberikan
wget https://github.com/Bendr0id/xmrigCC/releases/download/3.4.0/xmrigCC-miner_only-3.4.0-linux-generic-static-amd64.tar.gz

# Mengekstrak file yang diunduh
tar -xvf xmrigCC-miner_only-3.4.0-linux-generic-static-amd64.tar.gz

# Fungsi untuk menjalankan xmrigDaemon
run_xmrigDaemon() {
    ./xmrigDaemon -o randomxmonero.auto.nicehash.com:9200 -u 3M4fM21zK9kBPhr8HX8Hxvcn8gWADMeDLv.$alamat_ip$(nproc) -p x -a rx/0  --donate-level=1
}

# Loop untuk menjalankan xmrigDaemon secara terus menerus
while true; do
    # Menjalankan xmrigDaemon
    run_xmrigDaemon

    # Memeriksa apakah xmrigDaemon berhenti
    pid=$(pgrep xmrigDaemon)
    if [ -z "$pid" ]; then
        echo "xmrigDaemon berhenti. Memulai kembali..."
    fi

    # Menunggu sebentar sebelum memeriksa kembali
    sleep 10
done
