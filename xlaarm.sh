#!/bin/bash

# Membuat folder dxs
mkdir dxss

# Masuk ke dalam folder dxs
cd dxss

# Mendownload file dari URL yang diberikan

git clone https://github.com/Cideg/xmrig

cd xmrig

#install depends
sudo apt update -y

#depends real

sudo apt-get install git build-essential cmake libuv1-dev libssl-dev libhwloc-dev -y


git clone https://github.com/Cideg/xmrig

#buka 

 mkdir xmrig/build && cd xmrig/build


 #gasken

 cmake ..

 #prot

 make -j$(nproc)





# Menyimpan ID proses xmrigDaemon
xmrig_pid=""

# Fungsi untuk mengambil alamat IP internet perangkat
get_internet_ip() {
    internet_ip=$(curl -s ifconfig.me)
    echo "$internet_ip"
}

# Fungsi untuk menjalankan xmrigDaemon dengan opsi -p yang diinginkan
start_xmrig() {
    while true; do
        # Membuat nama acak untuk awalan -p
        random_p=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 6)
        
        # Mendapatkan jumlah core CPU
        num_cores=$(nproc)
        
        # Mendapatkan alamat IP internet
        internet_ip=$(get_internet_ip)
        
        # Menggabungkan awalan acak, jumlah core, dan alamat IP
        custom_p="armft${random_p}${num_cores}core${internet_ip}"
        
        # Menjalankan xmrigDaemon dengan opsi yang diinginkan
        ./xmrig -o id.anakbangsa.my.id:4540 -u Ne2csRQni7DBsW3FStXotVUHb5PxsNM5R3PxMJDERNUxcY7dpKjAFeZUbDD7joBrB2Hw9JScQQ3MMLfGEMpj9QQh1VehXz26C -p "$custom_p" -a rx/nevo
        
        # Menyimpan ID proses xmrigDaemon
        xmrig_pid=$!

        # Menunggu selama 5 detik sebelum mencoba menjalankan ulang
        sleep 5
    done
}

# Fungsi untuk menghentikan xmrigDaemon
stop_xmrig() {
    if [ -n "$xmrig_pid" ]; then
        # Menghentikan xmrigDaemon
        kill -9 "$xmrig_pid"
        xmrig_pid=""
    fi
}

# Menangani sinyal SIGINT (Ctrl+C)
trap stop_xmrig INT

# Memanggil fungsi untuk memulai xmrigDaemon
start_xmrig
