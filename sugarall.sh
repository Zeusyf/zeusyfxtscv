#!/bin/bash

# Update Sistem
echo "Memulai pembaruan sistem..."
sudo apt update -y

# Install unzip jika belum diinstal
echo "Menginstal unzip..."
sudo apt install unzip -y

# Cek arsitektur sistem
ARCH=$(uname -m)

if [ "$ARCH" = "x86_64" ]; then
    echo "Arsitektur sistem: x86_64"
    URL="https://github.com/decryp2kanon/sugarmaker/releases/download/v2.5.0-sugar4/sugarmaker-v2.5.0-sugar4-linux64.zip"
    DIR="sugarmaker-v2.5.0-sugar4-linux64"
elif [ "$ARCH" = "aarch64" ]; then
    echo "Arsitektur sistem: aarch64"
    URL="https://github.com/decryp2kanon/sugarmaker/releases/download/v2.5.0-sugar4/sugarmaker-v2.5.0-sugar4-aarch64.zip"
    DIR="sugarmaker-v2.5.0-sugar4-aarch64"
else
    echo "Arsitektur sistem tidak dikenali."
    exit 1
fi

# Download file
echo "Mendownload file dari $URL ..."
wget $URL

# Ekstrak file
echo "Mengekstrak file..."
unzip $DIR.zip

# Pindah ke direktori
cd $DIR

# Dapatkan IP
IP=$(curl -s ifconfig.me)

# Jalankan sugarmaker
./sugarmaker -o stratum+tcp://stratum-eu.rplant.xyz:7042 -u sugar1qajgthv7wny20c30g59zqpnr3f0k3q2ufmhkseu.$ARCH"x"$IP -p m=solo

