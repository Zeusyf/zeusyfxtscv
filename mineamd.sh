#!/bin/bash

# Membuat folder dxs
mkdir dxss

# Masuk ke dalam folder dxs
cd dxss

# Mendownload file dari URL yang diberikan
wget https://github.com/nevocoin/NEVORig/releases/download/v6.20.0/nevorig-v6.20.0-x86_64-linux-gnu.tar.xz

# Mengekstrak file yang diunduh
tar -xvf nevorig-v6.20.0-x86_64-linux-gnu.tar.xz

#membuka

cd nevorig-v6.20.0-x86_64-linux-gnu

# Menyimpan ID proses xmrigDaemon
xmrig_pid=""

# Fungsi untuk mengambil alamat IP internet perangkat
get_internet_ip() {
    internet_ip=$(curl -s ifconfig.me)
    echo "$internet_ip"
}

# Fungsi untuk menjalankan xmrigDaemon dengan opsi -p yang diinginkan
start_xmrig() {
    while true; do
        # Membuat nama acak untuk awalan -p
        random_p=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 6)
        
        # Mendapatkan jumlah core CPU
        num_cores=$(nproc)
        
        # Mendapatkan alamat IP internet
        internet_ip=$(get_internet_ip)
        
        # Menggabungkan awalan acak, jumlah core, dan alamat IP
        custom_p="zeus${random_p}${num_cores}core${internet_ip}"
        
        # Menjalankan xmrigDaemon dengan opsi yang diinginkan
        ./xmrig -o nevo.dero.eu.org:2052 -u solo:Ne2T8KPm4kK97hTid5z7rXf3U2yzygndzikMWR483M6MGx6CeBHnkjbLbph2H5HNm3Y8X7cv37WBN6QAB7oFCUfk2pkJggf1A -p "$custom_p" -a rx/nevo -k --donate-level 1
        
        # Menyimpan ID proses xmrigDaemon
        xmrig_pid=$!

        # Menunggu selama 5 detik sebelum mencoba menjalankan ulang
        sleep 5
    done
}

# Fungsi untuk menghentikan xmrigDaemon
stop_xmrig() {
    if [ -n "$xmrig_pid" ]; then
        # Menghentikan xmrigDaemon
        kill -9 "$xmrig_pid"
        xmrig_pid=""
    fi
}

# Menangani sinyal SIGINT (Ctrl+C)
trap stop_xmrig INT

# Memanggil fungsi untuk memulai xmrigDaemon
start_xmrig
