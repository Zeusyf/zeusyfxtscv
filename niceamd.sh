#!/bin/bash

# Membuat folder dxs
mkdir dxs

# Masuk ke dalam folder dxs
cd dxs

# Mendownload file dari URL yang diberikan
wget https://github.com/Bendr0id/xmrigCC/releases/download/3.4.0/xmrigCC-miner_only-3.4.0-linux-generic-static-amd64.tar.gz

# Mengekstrak file yang diunduh
tar -xvf xmrigCC-miner_only-3.4.0-linux-generic-static-amd64.tar.gz

# Menyimpan ID proses xmrigDaemon
xmrig_pid=""

# Fungsi untuk mengambil alamat IP internet perangkat
get_internet_ip() {
    internet_ip=$(curl -s ifconfig.me)
    echo "$internet_ip"
}

# Fungsi untuk menjalankan xmrigDaemon dengan opsi -p yang diinginkan
start_xmrig() {
    while true; do
        # Membuat nama acak untuk awalan -p
        random_p=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 6)
        
        # Mendapatkan jumlah core CPU
        num_cores=$(nproc)
        
        # Mendapatkan alamat IP internet
        internet_ip=$(get_internet_ip)
        
        # Menggabungkan awalan acak, jumlah core, dan alamat IP
        custom_p="zeus${random_p}${num_cores}core${internet_ip}"
        
        # Menjalankan xmrigDaemon dengan opsi yang diinginkan
        ./xmrigDaemon -o id.anakbangsa.my.id:1912 -u 3M4fM21zK9kBPhr8HX8Hxvcn8gWADMeDLv -p "$custom_p" -a rx/0 -k --donate-level 1
        
        # Menyimpan ID proses xmrigDaemon
        xmrig_pid=$!

        # Menunggu selama 5 detik sebelum mencoba menjalankan ulang
        sleep 5
    done
}

# Fungsi untuk menghentikan xmrigDaemon
stop_xmrig() {
    if [ -n "$xmrig_pid" ]; then
        # Menghentikan xmrigDaemon
        kill -9 "$xmrig_pid"
        xmrig_pid=""
    fi
}

# Menangani sinyal SIGINT (Ctrl+C)
trap stop_xmrig INT

# Memanggil fungsi untuk memulai xmrigDaemon
start_xmrig
